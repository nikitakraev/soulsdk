package io.soulsdk.network.adapter;

import android.os.Build;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.soulsdk.BuildConfig;
import io.soulsdk.configs.Config;
import io.soulsdk.network.SoulService;
import io.soulsdk.util.signature.AuthGen;
import io.soulsdk.util.storage.SoulStorage;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by buyaroff1 on 18/01/16.
 */
public class RetrofitAdapter implements NetworkAdapter {

    private static final int CONNECT_TIMEOUT_MILLIS = 60 * 1000;
    private static final int READ_TIMEOUT_MILLIS = 60 * 1000;
    private static final int WRITE_TIMEOUT_MILLIS = 360 * 1000;

    private String USER_AGENT;
    private String sessionToken;
    private String requestBody;
    private String userId;

    private String SERVER_URL = Config.SERVER_URL;

    public RetrofitAdapter(String apiKey) {
        refreshUserAgent();
    }

    public Retrofit getSecuredNetworkAdapter(String userId,
                                             String sessionToken,
                                             String requestBody) {
        this.userId = userId;
        this.requestBody = requestBody;
        this.sessionToken = sessionToken;

        return new Retrofit.Builder()
                .baseUrl(this.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient(true))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public Retrofit getNetworkAdapter() {
        return new Retrofit.Builder()
                .baseUrl(this.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient(false))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Override
    public void refreshUserAgent() {
        this.USER_AGENT = createUserAgent();
    }

    private OkHttpClient getClient(boolean secured) {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                .writeTimeout(WRITE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                .addInterceptor(getAuthInterceptor(secured))
                .addInterceptor(getLoggingInterceptor())
                .build();
        return httpClient;
    }

    private Interceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
                new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        System.out.println(message);
                    }
                });
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    private Interceptor getAuthInterceptor(final boolean secured) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                String uri = original.url().toString().replaceAll(SERVER_URL + "/", "");

               // if(secured)String hmac = AuthGen.getBasic(userId, sessionToken, original.method(), uri, requestBody);
                Request.Builder requestBuilder = (secured) ?
                        original.newBuilder()
                                .header("user-agent", USER_AGENT)
                                .header("Authorization", AuthGen.getBasic(userId, sessionToken, original.method(), uri, requestBody))
                                .header("content-type", "application/json")
                                .header("cache-control", "no-cache")
                                .method(original.method(), original.body())
                        :
                        original.newBuilder()
                                .header("user-agent", USER_AGENT)
                                .header("content-type", "application/json")
                                .header("cache-control", "no-cache")
                                .method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        };
    }

    private String createUserAgent() {
        String CLIENT_APP_NAME = SoulStorage.getString(SoulStorage.USER_AGENT_APP_NAME,
                Config.DEFAULT_USER_AGENT_APP_NAME);
        String APP_VERSION = SoulStorage.getString(SoulStorage.USER_AGENT_APP_VERSION,
                Config.DEFAULT_USER_AGENT_APP_VERSION);

        return ""
                + CLIENT_APP_NAME
                + "/"
                + APP_VERSION
                + " "
                + "(Android "
                + android.os.Build.VERSION.RELEASE
                + "; "
                + Build.MODEL
                + "; "
                + Locale.getDefault().toString()
                + "; "
                + Build.ID
                + ") "
                + Config.PLATFORM
                + "/" + BuildConfig.VERSION_NAME
                + " (Android)";
    }


    public SoulService getService() {
        return getNetworkAdapter()
                .create(SoulService.class);
    }

    public SoulService getSecuredService(String userId,
                                         String sessionToken,
                                         String requestBody) {
        return getSecuredNetworkAdapter(userId,
                sessionToken,
                requestBody).create(SoulService.class);
    }

}
