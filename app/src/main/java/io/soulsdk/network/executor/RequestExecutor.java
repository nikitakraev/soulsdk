package io.soulsdk.network.executor;

import io.soulsdk.model.general.SoulCallback;
import io.soulsdk.model.general.SoulError;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.network.SoulService;
import io.soulsdk.network.managers.Credentials;
import io.soulsdk.util.HttpUtils;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Buiarov on 04/03/16.
 */
public class RequestExecutor<T> {

    private static final String TAG = "RequestExecutor";

    private int ATTEMPTS_COUNT = 0;
    private int ATTEMPTS_AUTH_COUNT = -1;

    private ResponseParser<T> responseParser;
    private Credentials credentials;
    private SoulCallback soulCallback;
    private Observable<T> responseObservable;
    public SoulService soulService;
    private boolean isSearch = false;
    public Subscriber<? super SoulResponse<T>> mainSubscriber;


    public RequestExecutor(Observable<T> responseObservable, Credentials credentials) {
        this.responseObservable = responseObservable;
        this.credentials = credentials;
        this.soulCallback = null;
        responseParser = new ResponseParser<>(credentials, null, isSearch);
    }

    public RequestExecutor(Observable<T> responseObservable, Credentials credentials, SoulService soulService) {
        this.responseObservable = responseObservable;
        this.credentials = credentials;
        this.soulService = soulService;
        this.soulCallback = null;
        responseParser = new ResponseParser(credentials, null, isSearch);
    }

    public RequestExecutor(Observable<T> responseObservable, Credentials credentials,
                           SoulCallback soulCallback, boolean isSearch) {
        this.responseObservable = responseObservable;
        this.isSearch = isSearch;
        this.credentials = credentials;
        this.soulCallback = soulCallback;
        responseParser = new ResponseParser(credentials, null, isSearch);
    }

    public RequestExecutor(Observable<T> responseObservable,
                           Credentials credentials, SoulCallback soulCallback,
                           String authSource) {
        this.responseObservable = responseObservable;
        this.credentials = credentials;
        this.soulCallback = soulCallback;
        responseParser = new ResponseParser(credentials, authSource, isSearch);
    }

    public RequestExecutor(Observable<T> responseObservable,
                           Credentials credentials, SoulCallback soulCallback,
                           String authSource, String tempLogin, String tempPass) {
        this.responseObservable = responseObservable;
        this.credentials = credentials;
        this.soulCallback = soulCallback;
        responseParser = new ResponseParser(credentials, authSource, isSearch, tempLogin, tempPass);
    }

    public RequestExecutor(Observable<T> responseObservable, Credentials credentials,
                           SoulCallback soulCallback, boolean isSearch, boolean isLogout) {
        this.responseObservable = responseObservable;
        this.isSearch = isSearch;
        this.credentials = credentials;
        this.soulCallback = soulCallback;
        responseParser = new ResponseParser(credentials, null, isSearch, isLogout);
    }

    public Observable<SoulResponse<T>> asObservable() {
        if (soulCallback != null) {
            execute();
            return null;
        } else {
            return Observable.create(new Observable.OnSubscribe<SoulResponse<T>>() {
                @Override
                public void call(Subscriber<? super SoulResponse<T>> subscriber) {
                    RequestExecutor.this.makeRequest(responseObservable, subscriber);
                }
            });
        }
    }

    private void execute() {
        makeRequest(responseObservable, null);
    }

    private void makeRequest(Observable<T> responseObservable,
                             Subscriber<? super SoulResponse<T>> subscriber) {
        if (checkApiKeyExists(subscriber)) {
            this.mainSubscriber = subscriber;
            doRequest(responseObservable)
                    .retryWhen(new RetryOnConnectionErrorObservable())
                    .subscribe(
                            RequestExecutor.this::finalSuccess,
                            RequestExecutor.this::errorWrapper,
                            RequestExecutor.this::finalComplete
                    );
        }
    }

    private void errorWrapper(Throwable err) {
        err.printStackTrace();
        finalFail(HttpUtils.createSoulError(err, false));
    }

    private void finalSuccess(T res) {
        responseParser.parseResult(res);
        checkBooleanResult(res, true);
        if (soulCallback != null) soulCallback.onSuccess(res);
        if (mainSubscriber != null) mainSubscriber.onNext(new SoulResponse<T>(res));
    }

    private void finalFail(SoulError err) {
        checkBooleanResult(null, false);
        if (soulCallback != null) soulCallback.onError(err);
        if (mainSubscriber != null) mainSubscriber.onNext(new SoulResponse<T>(err));
    }

    private void finalComplete() {
        if (mainSubscriber != null) mainSubscriber.onCompleted();
    }

    private Observable<T> doRequest(final Observable<T> responseObservable) {
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(final Subscriber<? super T> sub) {
                responseObservable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(RequestExecutor.this::finalSuccess,
                                e -> RequestExecutor.this.responseFail(e, sub),
                                RequestExecutor.this::finalComplete);
            }
        });
    }

    private void responseFail(Throwable err, Subscriber<? super T> sub) {
        if (HttpUtils.isConnectionError(err) || HttpUtils.isLimitPerSecondError(err)) {
            onConnectionErrorProcess(err, sub);
        } /*else if (HttpUtils.isAuthError(err)) {
            onAuthErrorProcess(err);
        }*/ else {
            finalFail(HttpUtils.createSoulError(err, false));
        }
    }

    private void onConnectionErrorProcess(Throwable err, Subscriber<? super T> sub) {
        ATTEMPTS_COUNT++;
        if (ATTEMPTS_COUNT > RetryOnConnectionErrorObservable.ATTEMPTS_MAX_COUNT)
            finalFail(HttpUtils.createSoulError(err, true));
        else
            sub.onError(err);
    }

    private void onAuthErrorProcess(Throwable err) {
        ATTEMPTS_AUTH_COUNT++;
        if (ATTEMPTS_AUTH_COUNT > RetryOnConnectionErrorObservable.ATTEMPTS_MAX_COUNT)
            finalFail(HttpUtils.createSoulError(err, true));
        // else
        // loginViaPhone();
        //TODO implement subscribe logic after api will be ready
    }


    private T checkBooleanResult(T res, boolean value) {
        if (res == null || res instanceof Boolean) {
            return (T) new Boolean(value);
        } else return res;
    }

    private boolean checkApiKeyExists(Subscriber<? super SoulResponse<T>> subscriber) {
        // TODO add in production
        String msg = "SDK must be initialized. Set API KEY first";
       /* if (SoulStorage.getString(SoulStorage.API_KEY).isEmpty()) {
            subscriber.onNext(new SoulResponse<T>(
                    new SoulError(msg, SoulError.SDK_IS_NOT_INITIALIZED)));
            subscriber.onCompleted();
            Log.e(TAG, "checkApiKeyExists: " + msg);
            return false;
        }*/
        return true;
    }

 /*   public Observable<SoulResponse<AuthorizationResponse>> loginViaPhone() {
        SoulResponse<PhoneVerifyLoginREQ> phoneVerify = credentials.getPhoneVerifyREQ();
        if (phoneVerify.isErrorHappened()) {
            return Observable.just(new SoulResponse<>(phoneVerify.getError()));
        } else {
            return new RequestExecutor<>(
                    soulService.loginViaPhone(phoneVerify.getResponse()),
                    credentials)
                    .asObservable();
        }
    }*/
}