package io.soulsdk.network.executor;

import android.util.Log;

import io.soulsdk.model.dto.User;
import io.soulsdk.model.general.GeneralResponse;
import io.soulsdk.model.responses.AuthorizationResponse;
import io.soulsdk.model.responses.CurrentUserRESP;
import io.soulsdk.model.responses.EventsRESP;
import io.soulsdk.model.responses.UsersSearchRESP;
import io.soulsdk.network.managers.Credentials;
import io.soulsdk.network.managers.EventManager;
import io.soulsdk.network.managers.SearchResultManager;
import io.soulsdk.util.Constants;
import io.soulsdk.util.SoulVariableUtil;
import io.soulsdk.util.TimeUtils;
import io.soulsdk.util.storage.SoulStorage;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Buiarov on 24/03/16.
 */
public class ResponseParser<T> {

    private static final String TAG = "ResponseParser";

    private Credentials credentials;
    private String authSource;
    private boolean isSearch;
    private boolean isLogout = false;
    private String tempLogin;
    private String tempPass;

    public ResponseParser(Credentials credentials, String authSource, boolean isSearch) {
        this.credentials = credentials;
        this.authSource = authSource;
        this.isSearch = isSearch;
    }

    public ResponseParser(Credentials credentials, String authSource, boolean isSearch, boolean isLogout) {
        this.credentials = credentials;
        this.authSource = authSource;
        this.isSearch = isSearch;
        this.isLogout = isLogout;
        parseLogout();
    }

    public ResponseParser(Credentials credentials, String authSource, boolean isLogout,
                          String tempLogin, String tempPass) {
        this.credentials = credentials;
        this.authSource = authSource;
        this.isLogout = isLogout;
        this.tempLogin = tempLogin;
        this.tempPass = tempPass;
        parseLogout();
    }

    public void parseResult(T res) {
        if (isSearch) {
            parseSearch(res);
        } else {
            parseAuthorizedResponse(res);
        }
        parseMeResponse(res);
        parseServerTime(res);
        parseEvents(res);
    }

    public void parseAuthorizedResponse(T res) {
        if (res instanceof AuthorizationResponse) {
            try {
                credentials.saveSessionToken(
                        ((AuthorizationResponse) res).getAuthorization().getSessionToken());
            } catch (Exception e) {
                Log.e(TAG, "AuthorizationResponse is empty", e);
            }
            try {
                credentials.saveUserId(
                        ((AuthorizationResponse) res).getMe().getId());
            } catch (Exception e) {
                Log.e(TAG, "User ID is empty", e);
            }
            try {
                User me = ((AuthorizationResponse) res).getMe();
                try {
                    // TODO:  FIXME: 22.4.16 - If it is the first time, we don't have a location for user - check for null?
                    SoulVariableUtil.getUsersLocation(me).setUpdatedTime(
                            SoulStorage.getLong(SoulStorage.LAST_TIME_LOCATION_UPDATED));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SoulStorage.saveCurrentUser(((AuthorizationResponse) res).getMe());
            } catch (Exception e) {
                Log.e(TAG, "CurrentUser saving error", e);
            }
            if (authSource != null) credentials.saveLastSuccessfulLoginSource(authSource);
            //TODO TEMPORAL
            if (authSource == Constants.PASS_LOGIN_SOURCE) {
                SoulStorage.save(SoulStorage.TEMP_LOGIN, tempLogin);
                SoulStorage.save(SoulStorage.TEMP_PASS, tempPass);
            }
        }
    }

    public void parseSearch(T res) {
        if ((res instanceof UsersSearchRESP) && (((UsersSearchRESP) res).getUsers()) != null) {
            SearchResultManager.generateAndSavePageToken();
        }
    }

    public void parseMeResponse(T obj) {
        Observable.just(obj)
                .map(new Func1<T, Void>() {
                    @Override
                    public Void call(T obj1) {
                        return ResponseParser.this.parseMeResponse_(obj1);
                    }
                })
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void res) {
                        ResponseParser.this.voidMethod();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable e) {
                        ResponseParser.this.onError(e);
                    }
                });
    }

    private void voidMethod() {
    }

    public Void onError(Throwable e) {
        Log.e(TAG, "CurrentUser saving error", e);
        return null;
    }

    public Void parseMeResponse_(T res) {
        try {
            SoulStorage.saveCurrentUser(((CurrentUserRESP) res).getCurrentUser());
        } catch (Exception e) {
        }
        return null;
    }

    public void parseServerTime(T res) {
        if (res instanceof GeneralResponse) {
            try {
                TimeUtils.saveServerTime(
                        TimeUtils.formatTimeFromAPI(
                                ((GeneralResponse) res).getAdditionalInfo().getServerTime()));
            } catch (Exception e) {
                Log.e(TAG, "Server time parsing error", e);
            }
        }
    }

    public void parseEvents(T res) {
        if (res instanceof EventsRESP) {
            try {
                EventManager.saveLastEvent((EventsRESP) res);
            } catch (Exception e) {
                Log.e(TAG, "Events parsing error", e);
            }
        }
    }

    private void parseLogout() {
        if (isLogout) {
            credentials.clearCredentials();
        }
    }
}
