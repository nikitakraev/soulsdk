package io.soulsdk.network.managers;

import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.requests.PhoneRequestREQ;
import io.soulsdk.model.requests.PhoneVerifyLoginREQ;

/**
 * Created by Buiarov on 08/03/16.
 */
public interface Credentials {

    SoulResponse<PhoneRequestREQ> getPhoneRequestREQ(String phoneNumber);

    SoulResponse<PhoneVerifyLoginREQ> getPhoneVerifyREQ(String verificationCode);

    SoulResponse<PhoneVerifyLoginREQ> getPhoneVerifyREQ();

    void saveSessionToken(String sessionToken);

    void clearCredentials();

    String getUserId();

    String getSessionToken();

    void saveUserId(String userId);

    String getLastSuccessfulLoginSource();

    void saveLastSuccessfulLoginSource(String authSource);

    String getTempLogin();

    String getTempPass();
}
