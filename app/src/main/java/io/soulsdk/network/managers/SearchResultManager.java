package io.soulsdk.network.managers;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.UUID;

import io.soulsdk.util.storage.SoulStorage;

import static io.soulsdk.util.storage.SoulStorage.SEARCH_PAGE_TOKEN;
import static io.soulsdk.util.storage.SoulStorage.SEARCH_SESSION_TOKEN;

/**
 * Created by Buiarov on 15/03/16.
 */
public class SearchResultManager {

    //TODO check tokens after reinstalling the application (From response of nextResults())

    public static class SearchCredentials {
        public String pageToken;
        public String sessionToken;

        public SearchCredentials(String pageToken, String sessionToken) {
            this.pageToken = pageToken;
            this.sessionToken = sessionToken;
        }
    }

    public static void refreshSearchResult() {
        SoulStorage.save(SEARCH_SESSION_TOKEN, "");
        SoulStorage.save(SEARCH_PAGE_TOKEN, "");
    }

    public static SearchCredentials getNextUsersSearchResult() {
        return new SearchCredentials(getCurrentPageToken(), getCurrentSessionToken());
    }

    private static String getCurrentPageToken() {
        String sessionToken = SoulStorage.getString(SEARCH_PAGE_TOKEN);
        if (sessionToken.isEmpty()) {
            sessionToken = generateAndSavePageToken();
        }
        return sessionToken;
    }
    private static String getCurrentSessionToken() {
        String sessionToken = SoulStorage.getString(SEARCH_SESSION_TOKEN);
        if (sessionToken.isEmpty()) {
            sessionToken = generateAndSaveSessionToken();
        }
        return sessionToken;
    }

    public static String generateAndSavePageToken() {
        String token = UUID.randomUUID().toString();
        SoulStorage.save(SEARCH_PAGE_TOKEN, token);
        return token;
    }

    public static String generateAndSaveSessionToken() {
        String token = new BigInteger(130, new SecureRandom()).toString(32);
        SoulStorage.save(SEARCH_SESSION_TOKEN, token);
        return token;
    }

}
