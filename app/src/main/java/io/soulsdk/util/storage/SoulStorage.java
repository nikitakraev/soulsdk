package io.soulsdk.util.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import io.soulsdk.model.dto.User;

/**
 * Created by Buiarov on 17/03/16.
 */
public class SoulStorage {

    public static final String DEFAULT_STRING = "";
    public static final long DEFAULT_LONG = -1;

    public static final String USER_ID = "USER_ID";
    public static final String API_KEY = "API_KEY";
    public static final String SESSION_TOKEN = "SESSION_TOKEN";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String VERIFICATION_CODE = "VERIFICATION_CODE";

    public static final String SEARCH_PAGE_TOKEN = "SEARCH_PAGE_TOKEN";
    public static final String SEARCH_SESSION_TOKEN = "SEARCH_SESSION_TOKEN";

    public static final String SERVER_TIME_DIFF = "SERVER_TIME_DIFF";
    public static final String USERS_LIFETIME_VALUE = "USERS_LIFETIME_VALUE";
    public static final String GCM_SENDER_ID = "GCM_SENDER_ID";
    public static final String LIKE_LIFETIME = "LIKE_LIFETIME";
    public static final String DISLIKE_LIFETIME = "DISLIKE_LIFETIME";
    public static final String BLOCK_LIFETIME = "BLOCK_LIFETIME";
    public static final String EVENTS_LIMIT = "EVENTS_LIMIT";

    public static final String SUCCESSFUL_LOGIN_SOURCE = "SUCCESSFUL_LOGIN_SOURCE";
    public static final String CURRENT_USER = "CURRENT_USER";
    public static final String LAST_TIME_LOCATION_UPDATED = "LAST_TIME_LOCATION_UPDATED";

    public static final String USER_AGENT_APP_NAME = "USER_AGENT_APP_NAME";
    public static final String USER_AGENT_APP_VERSION = "USER_AGENT_APP_VERSION";

    public static final String PUBLISH_KEY = "PUBLISH_KEY";
    public static final String SUBSCRIBE_KEY = "SUBSCRIBE_KEY";

    public static final String CHAT_MESSAGE_PREFIX = "CHAT_";
    public static final String CHAT_ID_PREFIX = "CHAT_ID_";
    public static final String CHAT_MESSAGES_UNSENT = "CHAT_MESSAGES_QUEUE";

    public static final String LAST_EVENT_RECORD_ID = "LAST_EVENT_RECORD_ID";

    public static final String TEMP_LOGIN = "TEMP_LOGIN";
    public static final String TEMP_PASS = "TEMP_PASS";

    private static Context context;

    public static void initialize(String apiKey, Context ctx) {
        context = ctx;
        save(API_KEY, apiKey);
    }

    public static void save(String key, String val) {
        assertContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(context.getPackageName() + key, val);
        editor.apply();
    }

 /*   public static void save(String key, long val) {
        save(key, String.valueOf(val));
    }*/

    public static void saveLong(String key, long val) {
        assertContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(context.getPackageName() + key, val);
        editor.apply();
    }
    public static void saveInteger(String key, int val) {
        assertContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(context.getPackageName() + key, val);
        editor.apply();
    }


    public static String getString(String key, String defaultValue) {
        assertContext();
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(context.getPackageName() + key, defaultValue);
    }

    public static String getString(String key) {
        return getString(key, DEFAULT_STRING);
    }

    public static long getLong(String key) {
        assertContext();
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getLong(context.getPackageName() + key, DEFAULT_LONG);
    }

    public static long getLong(String key, long defaultValue) {
        assertContext();
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getLong(context.getPackageName() + key, defaultValue);
    }
    public static int getInt(String key, int defaultValue) {
        assertContext();
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(context.getPackageName() + key, defaultValue);
    }

    private static void assertContext() {
        //TODO add in production
        // if (context == null) throw new Error("SoulStorage: SoulSDK is not initialized!");
    }

    public static void saveCurrentUser(User user) {
        assertContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();

        if(user==null) {
            editor.putString(CURRENT_USER, "");
        } else {
            Gson gson = new Gson();
            String json = gson.toJson(user);
            editor.putString(CURRENT_USER, json);
        }
        editor.commit();
    }

    public static User getUser() {
        assertContext();
        try {
            Gson gson = new Gson();
            String json = PreferenceManager.getDefaultSharedPreferences(context)
                    .getString(CURRENT_USER, DEFAULT_STRING);
            return gson.fromJson(json, User.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }


}
