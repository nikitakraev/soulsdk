package io.soulsdk.util;

import com.google.gson.Gson;

import io.soulsdk.model.dto.User;
import io.soulsdk.model.dto.UsersLocation;

/**
 * //
 *
 * @author Buiarov Iurii
 * @version 0.15
 * @since 05/04/16
 */
public class SoulVariableUtil {

    public static UsersLocation getUsersLocation(User user) throws Exception{
        return new Gson().fromJson(user.getParameters().getFilterableAsJson().get("location"), UsersLocation.class);
    }

    public static Long getUsersAvailableTill(User user) throws Exception{
        return new Gson().fromJson(user.getParameters().getFilterableAsJson().get("availableTill"), Long.class);
    }

    public static String getUsersGender(User user) throws Exception{
        return new Gson().fromJson(user.getParameters().getFilterableAsJson().get("gender"), String.class);
    }

    public static String getUsersTargetGender(User user) throws Exception{
        return new Gson().fromJson(user.getParameters().getFilterableAsJson().get("lookingForGender"), String.class);
    }
}