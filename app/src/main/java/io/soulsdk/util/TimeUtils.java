package io.soulsdk.util;

import io.soulsdk.util.storage.SoulStorage;

import static io.soulsdk.util.storage.SoulStorage.SERVER_TIME_DIFF;

/**
 * Created by buyaroff1 on 22/01/16.
 */
public class TimeUtils {

    public static Double formatTimeToAPI(long time) {
        return Double.valueOf(time / 1000);
    }

    public static long formatTimeFromAPI(double time) {
        return (long) (time * 1000);
    }

    public static void saveServerTime(long serverTime) {
        SoulStorage.saveLong(SERVER_TIME_DIFF, System.currentTimeMillis() - serverTime);
    }
}
