package io.soulsdk.sdk;

import android.util.Log;

import io.soulsdk.configs.Config;
import io.soulsdk.network.adapter.NetworkAdapter;
import io.soulsdk.util.storage.SoulStorage;

import static io.soulsdk.util.storage.SoulStorage.BLOCK_LIFETIME;
import static io.soulsdk.util.storage.SoulStorage.DISLIKE_LIFETIME;
import static io.soulsdk.util.storage.SoulStorage.GCM_SENDER_ID;
import static io.soulsdk.util.storage.SoulStorage.LIKE_LIFETIME;
import static io.soulsdk.util.storage.SoulStorage.PUBLISH_KEY;
import static io.soulsdk.util.storage.SoulStorage.SUBSCRIBE_KEY;
import static io.soulsdk.util.storage.SoulStorage.USERS_LIFETIME_VALUE;
import static io.soulsdk.util.storage.SoulStorage.USER_AGENT_APP_NAME;
import static io.soulsdk.util.storage.SoulStorage.*;

/**
 * Provides a list of methods for initial settings and configuration of SDK. All settings are
 * not required. There are default values for all of these settings.
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */
public class SoulConfigs {

    public static final long HUNDRED_YEARS = 100 * 365 * 24 * 60 * 60;
    public static final long ONE_YEAR = 365 * 24 * 60 * 60;
    public static final long HALF_YEAR = 183 * 24 * 60 * 60;
    public static final long ONE_MONTH = 30 * 24 * 60 * 60;
    public static final long ONE_WEEK = 7 * 24 * 60 * 60;
    public static final long ONE_DAY = 24 * 60 * 60;
    public static final long ONE_HOUR = 60 * 60;


    private static final String TAG = "SoulConfigs";

    private static NetworkAdapter adapter;

    private SoulConfigs() {
    }

    static void initialize(NetworkAdapter ad) {
        adapter = ad;
    }

    /**
     * Sets search lifetime value. It means the value availableTill of
     * {@link io.soulsdk.model.dto.Filterable} of {@link SoulCurrentUser} will be set as
     * [ServerTime + this value] each time after {@link SoulCurrentUser#turnSearchOn()}
     * will be called.
     * The value [availableTill] is time until which User will have possibility to search other
     * users, and other users will be able to find current user. This setting is not required.
     * There is default value USERS_LIFETIME_VALUE.
     *
     * @param val value in milliseconds
     */
    public static void setSearchLifeTimeValue(long val) {
        SoulStorage.saveLong(USERS_LIFETIME_VALUE, val);
    }

    public static void setGCMSenderId(String id) {
        SoulStorage.save(GCM_SENDER_ID, id);
    }

    public static String getGCMSenderId() {
        return SoulStorage.getString(GCM_SENDER_ID);
    }

    /**
     * Retrieve search lifetime value.
     *
     * @return the search life time value. If it was not being set before USERS_LIFETIME_VALUE will be
     * returned.
     */
    static long getSearchLifeTimeValue() {
        return SoulStorage.getLong(USERS_LIFETIME_VALUE, Config.DEFAULT_USERS_LIFETIME);
    }

    /**
     * Sets lifetime for all existing reactions (like, dislike, block etc.)
     *
     * @param val value in milliseconds. This setting is not required.
     *            There is default value DEFAULT_REACTION_LIFE_TIME.
     */
    public static void setAllReactionsLifeTime(long val) {
        SoulStorage.saveLong(LIKE_LIFETIME, val);
        SoulStorage.saveLong(DISLIKE_LIFETIME, val);
        SoulStorage.saveLong(BLOCK_LIFETIME, val);
    }

    /**
     * Sets lifetime for "like" reaction.
     *
     * @param val value in milliseconds. This setting is not required.
     *            There is default value DEFAULT_LIKE_REACTION_LIFE_TIME.
     */
    public static void setLikeReactionLifeTime(long val) {
        SoulStorage.saveLong(LIKE_LIFETIME, val);
    }

    /**
     * Retrieve Like-reaction lifetime value.
     *
     * @return the Like-reaction lifetime value. If it was not being set before
     * DEFAULT_LIKE_REACTION_LIFE_TIME will be returned.
     */
    public static long getLikeReactionLifeTime() {
        return SoulStorage.getLong(LIKE_LIFETIME, Config.DEFAULT_LIKE_REACTION_LIFE_TIME);
    }

    /**
     * Sets lifetime for "dislike" reaction.
     *
     * @param val value in milliseconds. This setting is not required.
     *            There is default value DEFAULT_DISLIKE_REACTION_LIFE_TIME.
     */
    public static void setDislikeReactionLifeTime(long val) {
        SoulStorage.saveLong(DISLIKE_LIFETIME, val);
    }

    /**
     * Retrieve Dislike-reaction lifetime value.
     *
     * @return the Dislike-reaction lifetime value. If it was not being set before
     * DEFAULT_DISLIKE_REACTION_LIFE_TIME will be returned.
     */
    public static long getDislikeReactionLifeTime() {
        return SoulStorage.getLong(DISLIKE_LIFETIME, Config.DEFAULT_DISLIKE_REACTION_LIFE_TIME);
    }

    /**
     * Sets lifetime for "block" reaction.
     *
     * @param val value in milliseconds. This setting is not required.
     *            There is default value DEFAULT_BLOCK_REACTION_LIFE_TIME.
     */
    public static void setBlockReactionLifeTime(long val) {
        SoulStorage.saveLong(BLOCK_LIFETIME, val);
    }

    /**
     * Retrieve Block-reaction lifetime value.
     *
     * @return the Block-reaction lifetime value. If it was not being set before
     * DEFAULT_BLOCK_REACTION_LIFE_TIME will be returned.
     */
    public static long getBlockReactionLifeTime() {
        return SoulStorage.getLong(BLOCK_LIFETIME, Config.DEFAULT_BLOCK_REACTION_LIFE_TIME);
    }

    /**
     * Sets app name for User-Agent of HTTP-requests
     *
     * @param val short name of application for sending as a parameter in User-Agent of
     *            HTTP-requests. This setting is not required. There is default value
     *            DEFAULT_USER_AGENT_APP_NAME.
     */
    public static void setUserAgentAppName(String val) {
        SoulStorage.save(USER_AGENT_APP_NAME, val);
        if (checkInit()) adapter.refreshUserAgent();
    }

    /**
     * Sets app version for User-Agent of HTTP-requests
     *
     * @param val version of application for sending as a parameter in User-Agent of
     *            HTTP-requests. This setting is not required. There is default value
     *            DEFAULT_USER_AGENT_APP_VERSION.
     */
    public static void setUserAgentAppVersion(String val) {
        SoulStorage.save(USER_AGENT_APP_VERSION, val);
        if (checkInit()) adapter.refreshUserAgent();
    }

    public static void setChatsCredentials(String publishKey, String subscribeKey) {
        SoulStorage.save(PUBLISH_KEY, publishKey);
        SoulStorage.save(SUBSCRIBE_KEY, subscribeKey);
    }


    private static boolean checkInit() {
        if (adapter == null) {
            Log.e(TAG, "checkApiKeyExists: " + " SDK mast be initialized");
            return false;
        }
        return true;
    }


    public static void setEventsLimit(int val) {
        SoulStorage.saveInteger(EVENTS_LIMIT, val);
    }

    public static int getEventsLimit() {
        return SoulStorage.getInt(EVENTS_LIMIT, Config.DEFAULT_EVENTS_LIMIT);
    }

}
