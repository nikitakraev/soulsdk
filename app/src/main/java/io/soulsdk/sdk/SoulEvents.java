package io.soulsdk.sdk;

import io.soulsdk.model.general.SoulCallback;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.responses.EventsRESP;
import io.soulsdk.network.managers.EventManager;
import io.soulsdk.sdk.helpers.ServerAPIHelper;
import rx.Observable;

/**
 * <p>
 * Provides a list of methods for working with Events.
 * </p>
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */
public class SoulEvents {

    private static ServerAPIHelper helper;

    private SoulEvents() {
    }

    static void initialize(ServerAPIHelper hp) {
        helper = hp;
    }

    /**
     * Returns latest events sorted by id
     *
     * @param sinceDate    if set, returns only items with time larger than since. Can be null
     * @param after        if set, returns only items with id larger than after.  Can be null
     * @param limit        specifies how many results to return (default: 20; min: 1; maximum: 100))
     * @param soulCallback general {@link SoulCallback} of {@link EventsRESP}
     */
    public static void get(Long sinceDate, Integer after, Integer limit, SoulCallback<EventsRESP> soulCallback) {
        helper.getEvents(sinceDate, after, limit, soulCallback);
    }

    /**
     * Returns latest events sorted by id
     *
     * @param sinceDate if set, returns only items with time larger than since. Can be null
     * @param after     if set, returns only items with id larger than after.  Can be null
     * @param limit     specifies how many results to return (default: 20; min: 1; maximum: 100))
     * @return observable of general {@link SoulResponse} of {@link EventsRESP}
     */
    public static Observable<SoulResponse<EventsRESP>> get(Long sinceDate, Integer after, Integer limit) {
        return helper.getEvents(sinceDate, after, limit, null);
    }

    public static void get(SoulCallback<EventsRESP> soulCallback) {
        helper.getEvents(null, EventManager.getLastEventRecordId(), SoulConfigs.getEventsLimit(), soulCallback);
    }

    public static Observable<SoulResponse<EventsRESP>> get() {
        return helper.getEvents(null, EventManager.getLastEventRecordId(), SoulConfigs.getEventsLimit(), null);
    }
}