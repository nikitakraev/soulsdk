package io.soulsdk.sdk.test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.soulsdk.model.dto.Chat;
import io.soulsdk.model.dto.ChatDeliveryMessage;
import io.soulsdk.model.dto.ChatMessage;
import io.soulsdk.model.dto.ChatReadMessage;
import io.soulsdk.model.dto.Meta;
import io.soulsdk.model.dto.UsersLocation;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.responses.ChatsRESP;
import io.soulsdk.sdk.SoulCommunication;
import io.soulsdk.sdk.SoulCurrentUser;
import io.soulsdk.sdk.SoulSystem;
import io.soulsdk.sdk.helpers.chat.ChatsHelper;
import io.soulsdk.util.storage.SoulStorage;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author kitttn
 */
public class TestSoulCommunication {
	final static String TEST_PUBLISH_KEY = "pub-c-a03aa06d-101f-45ba-8e62-69831b484de9";
	final static String TEST_SUBSCRIBE_KEY = "sub-c-dc24d4f2-f51e-11e5-861b-02ee2ddab7fe";

	private Random random = new Random();
	Gson gson = new Gson();
	private List<Chat> allChats;
	private static ChatsHelper helper;
	private static TestSoulCommunication instance;

	private static String userId = "";
	private static String gcmId = "";

	// ===================== public methods =====================
	public static Observable<SoulResponse<ChatsRESP>> getAll(Integer offset, Integer limit, Boolean showExpired) {
		return getInstance().loadAllChats();
	}

	public static Observable<SoulResponse<ChatMessage>> connectToChat(String channelName, String partnerId) {
		return getInstance().connect(channelName, partnerId);
	}

	public static Observable<SoulResponse<Boolean>> delete(String channelName) {
		System.out.println("deleting chat...");
		getInstance()._delete(channelName);
		return Observable.just(new SoulResponse<>(true));
	}

	public static void createChat(Chat chat) {
		getInstance()._createChat(chat);
	}

	public static void markMessageDelivered(String channelName, ChatMessage deliveredMessage) {
		ChatDeliveryMessage message = new ChatDeliveryMessage(deliveredMessage);
		helper.sendMessage(channelName, message);
		System.out.println("Sending delivery confirmation: " + message);
		System.out.println("Message type: " + message.getType());
	}

	public static void markMessageRead(String channelName, ChatMessage readMessage) {
		long time = SoulSystem.getServerTime();
		ChatReadMessage msg = new ChatReadMessage(readMessage, time);
		helper.sendMessage(channelName, msg);
		System.out.println("Sending read confirmation: " + msg);
		System.out.println("Message type: " + msg.getType());
	}

	// ======================= private methods ==================

	private Observable<SoulResponse<ChatsRESP>> loadAllChats() {
		//sleep(5000);

		ChatsRESP resp = new ChatsRESP();
		resp.setChats(allChats);
		resp.set_meta(new Meta());
		return Observable.just(new SoulResponse<>(resp));
	}

	private Observable<SoulResponse<ChatMessage>> connect(String channelName, String partnerId) {
		return helper.connectAndListen(channelName, partnerId, null)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeOn(Schedulers.io());
	}

	private void _createChat(Chat chat) {
		for (Chat c : allChats)
			if (c.getId().equals(chat.getChannelName()))
				return;

		allChats.add(chat);
		serializeHistory();
	}

	private void _delete(String channelName) {
		SoulStorage.save(SoulStorage.CHAT_ID_PREFIX + channelName, "-1");
		SoulStorage.save(SoulStorage.CHAT_MESSAGE_PREFIX + channelName, "{}");

		helper.unsubscribe(channelName);

		for (int i = 0; i < allChats.size(); ++i)
			if (allChats.get(i).getChannelName().equals(channelName)) {
				allChats.remove(i);
				serializeHistory();
				return;
			}
	}

	// messages

	public static void sendChatMessage(String channelName, String message) {
		helper.sendMessage(channelName, message);
	}

	public static void sendChatPhoto(String channelName, File file) {
		helper.sendMessage(channelName, file);
	}

	public static void sendChatLocation(String channelName) {
		UsersLocation location = new UsersLocation(55.43044, 27.402324);
		helper.sendMessage(channelName, location);
	}

	// ======================== handling methods =====================

	private TestSoulCommunication() {
		if (userId.equals(""))
			throw new Error("Please, call init() first!");

		helper = new ChatsHelper(userId, gcmId, TEST_PUBLISH_KEY, TEST_SUBSCRIBE_KEY);
		deserialize();
	}

	public static void init(String userId, String gcmClientId) {
		TestSoulCommunication.userId = userId;
		TestSoulCommunication.gcmId = gcmClientId;
	}

	private static TestSoulCommunication getInstance() {
		if (instance == null)
			instance = new TestSoulCommunication();
		return instance;
	}

	private void sleep(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void serializeHistory() {
		SoulStorage.save("msg_history", gson.toJson(allChats));
	}

	private void deserialize() {
		String serialized = SoulStorage.getString("msg_history", "[]");
		TypeToken typeToken = new TypeToken<List<Chat>>() {
		};
		allChats = gson.fromJson(serialized, typeToken.getType());
		System.out.println("Loaded chats: " + allChats);
	}
}
