package io.soulsdk.sdk.helpers.chat;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubException;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import io.soulsdk.model.dto.ChatMessage;
import io.soulsdk.util.storage.SoulStorage;

/**
 * @author kitttn
 */
public class MessageSender {
	private static final String TAG = "MessageSender";
	public static final int ACTIVE_MODE_DELAY_MS = 200;
	public static final int PASSIVE_MODE_DELAY_MS = 1500;
	public static final int ACTIVE_MODE_TIMEOUT_MS = 30000;
	private long activeStartTime = 0;

	private final ConcurrentLinkedQueue<MessageToSend> queue;// = new ConcurrentLinkedQueue<>();
	private final ConcurrentHashMap<String, Boolean> allowed = new ConcurrentHashMap<>();
	private Gson gson = new Gson();
	private CurrentMode currentMode = CurrentMode.PASSIVE;
	private MessageSenderThread mainThread;
	private final Pubnub messageSender;

	private enum CurrentMode { ACTIVE, PASSIVE }

	private class MessageToSend {
		@SerializedName("m") private ChatMessage body;
		@SerializedName("a") private String adressee;

		MessageToSend(String adressee, ChatMessage msg) {
			body = msg;
			this.adressee = adressee;
		}

		@Override
		public String toString() {
			return "{\"m\": " + body.toString() + ", \"a\": \"" + adressee + "\"}";
		}
	}

	private class MessageSenderThread extends Thread {
		private boolean isRunning = false;

		@Override
		public void run() {
			try {
				runUnsafe();
			} catch (Exception e) {
			}
		}

		@Override
		public synchronized void start() {
			super.start();
			isRunning = true;
		}

		public synchronized void end() {
			isRunning = false;
		}

		public void runUnsafe() throws Exception {
			while (isRunning) {
				MessageToSend msg = queue.peek();
				while (msg == null || !isSendingAllowed(msg)) {
					sleep(currentMode == CurrentMode.ACTIVE ? ACTIVE_MODE_DELAY_MS : PASSIVE_MODE_DELAY_MS);
					//Log.i(TAG, "runUnsafe: Sleeping 8000 ms...");
					msg = queue.peek();
					if (switchModeToPassive())
						currentMode = CurrentMode.PASSIVE;
				}

				if (switchModeToPassive())
					currentMode = CurrentMode.PASSIVE;

				Log.i(TAG, "runUnsafe: Wow, have something to push!");
				Sender thread = new Sender(msg);
				thread.start();
				thread.join();
			}
		}
	}

	private class Sender extends Thread {
		private MessageToSend msg;
		public Sender(MessageToSend msg) {
			this.msg = msg;
		}

		@Override
		public void run() {
			allowed.put(msg.adressee, false);
			messageSender.publish(msg.adressee, msg.body.toJson(), new ChatCallback<>((channel, object) -> {
				queue.remove(msg);
				Log.i(TAG, "runUnsafe: Msg sender response: " + object);
				allowed.put(msg.adressee, true);
				saveQueue();
			}));
		}
	}

	// True async message sender in another thread
	public MessageSender(Pubnub sender) {
		queue = loadSavedQueue();
		messageSender = sender;
		mainThread = new MessageSenderThread();

		Log.i(TAG, "MessageSender:  >> Restoring send auth... <<");
		// This piece of code makes sure that queue will not block, trying to deliver messages
		for (MessageToSend msg : queue) {
			setSendingAllowed(true, msg.adressee);
		}
		Log.i(TAG, "MessageSender:  >> Queue loaded. <<");
	}

	public void send(String channelName, ChatMessage message) {
		queue.offer(new MessageToSend(channelName, message));
		currentMode = CurrentMode.ACTIVE;
		activeStartTime = System.nanoTime();
		Log.i(TAG, "send: Okay, message added to queue; Queue size: " + queue.size());
		saveQueue();
	}

	public boolean isSendingAllowed(MessageToSend msg) {
		if (msg == null)
			return false;
		String chatId = msg.adressee;
		return allowed.get(chatId) == null ? false : allowed.get(chatId);
	}

	public void terminate() {
		Log.i(TAG, "terminate: Terminating...");
		mainThread.end();
	}

	public void setSendingAllowed(boolean sendingAllowed, String channelName) {
		Log.i(TAG, "setSendingAllowed: " + sendingAllowed + " to " + channelName);
		if (!mainThread.isAlive() && sendingAllowed) {
			mainThread.start();
		}
		allowed.put(channelName, sendingAllowed);

		if (!sendingAllowed) {
			// if everything is ended, end mainThread
			boolean stillWorking = false;
			for (Boolean val : allowed.values())
				stillWorking = stillWorking || val;

			Log.i(TAG, "setSendingAllowed: I am still working? " + stillWorking);

			if (!stillWorking)
				terminate();
		}
	}

	private boolean switchModeToPassive() {
		return (System.nanoTime() - activeStartTime) / 1000 > ACTIVE_MODE_TIMEOUT_MS;
	}

	private ConcurrentLinkedQueue<MessageToSend> loadSavedQueue() {
		String unsentMessages = SoulStorage.getString(SoulStorage.CHAT_MESSAGES_UNSENT, "[]");
		Type type = new TypeToken<ConcurrentLinkedQueue<MessageToSend>>() {}.getType();

		ConcurrentLinkedQueue<MessageToSend> q = gson.fromJson(unsentMessages, type);
		Log.i(TAG, "MessageSender: initialized queue: " + q);

		return q;
	}

	private void saveQueue() {
		String unsentQueue = queue.toString();
		Log.i(TAG, "send: Saving queue: " + unsentQueue);
		SoulStorage.save(SoulStorage.CHAT_MESSAGES_UNSENT, unsentQueue);
	}
}
