package io.soulsdk.sdk.helpers.chat;

import com.pubnub.api.Callback;
import com.pubnub.api.PubnubError;

/**
 * @author kitttn
 */
public class ChatCallback<T> extends Callback {
	private ChannelInterface<String, T> onResponse;
	private ChannelInterface<String, PubnubError> onError;
	private ChannelInterface<String, Object> onConnect;
	private ChannelInterface<String, Object> onReconnect;
	private ChannelInterface<String, Object> onDisconnect;

	@Override
	public void successCallback(String channel, Object message) {
		if (onResponse != null) try {
				onResponse.execute(channel, (T) message);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	@Override
	public void errorCallback(String channel, PubnubError error) {
		if (onError != null) try {
				onError.execute(channel, error);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	@Override
	public void connectCallback(String channel, Object message) {
		if (onConnect != null) try {
			onConnect.execute(channel, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reconnectCallback(String channel, Object message) {
		if (onReconnect != null) try {
			onReconnect.execute(channel, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void disconnectCallback(String channel, Object message) {
		if (onDisconnect != null) try {
			onDisconnect.execute(channel, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ChatCallback() {}

	public ChatCallback(ChannelInterface<String, T> onResponse) {
		this.onResponse = onResponse;
	}

	public ChatCallback(ChannelInterface<String, T> onResponse,
						ChannelInterface<String, PubnubError> onError) {
		this.onResponse = onResponse;
		this.onError = onError;
	}

	public ChatCallback(ChannelInterface<String, T> onResponse,
						ChannelInterface<String, PubnubError> onError,
						ChannelInterface<String, Object> onConnect,
						ChannelInterface<String, Object> onDisconnect) {
		this.onResponse = onResponse;
		this.onError = onError;
		this.onConnect = onConnect;
		this.onDisconnect = onDisconnect;
	}

	public ChatCallback(ChannelInterface<String, T> onResponse,
						ChannelInterface<String, PubnubError> onError,
						ChannelInterface<String, Object> onConnect,
						ChannelInterface<String, Object> onReconnect,
						ChannelInterface<String, Object> onDisconnect) {
		this.onResponse = onResponse;
		this.onError = onError;
		this.onConnect = onConnect;
		this.onReconnect = onReconnect;
		this.onDisconnect = onDisconnect;
	}
}
