package io.soulsdk.sdk.helpers.chat;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.soulsdk.model.dto.ChatLocationMessage;
import io.soulsdk.model.dto.ChatMessage;
import io.soulsdk.model.dto.ChatPhotoMessage;
import io.soulsdk.model.dto.UsersLocation;
import io.soulsdk.model.general.SoulCallback;
import io.soulsdk.model.general.SoulError;
import io.soulsdk.model.general.SoulResponse;
import io.soulsdk.model.responses.PhotoRESP;
import io.soulsdk.sdk.SoulMedia;
import io.soulsdk.sdk.SoulSystem;
import io.soulsdk.util.storage.SoulStorage;
import rx.Observable;
import rx.Statement;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

/**
 * @author kitttn
 */
public class ChatsHelper {
	private static final String TAG = "ChatsHelper";
	private final Pubnub chatManager;
	private final String userId;
	private final HashMap<String, PublishSubject<SoulResponse<ChatMessage>>> inputChannels = new HashMap<>();
	private final HashMap<String, MessageQueue<ChatMessage>> cache = new HashMap<>();
	private final HashMap<String, String> partners = new HashMap<>();
	private final MessageSender sender;

	private long myLastMessageId = -1;
	private ChatMessage savedMessage;
	private boolean loadMore = true;

	public ChatsHelper(String userId, String pushNotificationKey, String publishKey, String subscribeKey) {
		chatManager = new Pubnub(publishKey, subscribeKey);
		this.userId = userId;
		chatManager.setUUID(userId);
		chatManager.enablePushNotificationsOnChannel(userId, pushNotificationKey);
		Log.i(TAG, "ChatsHelper: Enabled GCM, channel: " + userId);

		sender = new MessageSender(chatManager);
	}

	public Observable<SoulResponse<ChatMessage>> connectAndListen(final String channelName, String partnerId, SoulCallback<ChatMessage> callback) {
		// subscribe, but do not throw messages to user, just cache them
		cache.put(channelName, new MessageQueue<ChatMessage>());
		partners.put(channelName, partnerId);
		handleSubscription(channelName);

		// SoulStorage.save(SoulStorage.CHAT_MESSAGE_PREFIX + chatId, "{}");
		// SoulStorage.save(SoulStorage.CHAT_ID_PREFIX + chatId, "-1");
		String tmp = SoulStorage.getString(SoulStorage.CHAT_MESSAGE_PREFIX + channelName, "{}");
		savedMessage = ChatMessage.createFromJson(tmp);
		tmp = SoulStorage.getString(SoulStorage.CHAT_ID_PREFIX + channelName, "-1");
		myLastMessageId = Long.parseLong(tmp);

		Log.i(TAG, "connectAndListen: Saved message: " + savedMessage);
		Log.i(TAG, "connectAndListen: Saved id of my last message: " + myLastMessageId);

		PublishSubject<SoulResponse<ChatMessage>> subject = getInputChannel(channelName);
		Counter counter = new Counter();
		Statement.doWhile(
				Observable.just(true)
						.concatMap(b -> getMessageHistory(channelName, counter)
								.filter(msg -> filterAndNotify(subject, msg))
								.map(SoulResponse::getResponse)
								.buffer(counter.count))
						.map(msg -> cache(channelName, msg)),
				() -> {
					boolean res = needLoadMoreMessages(counter);
					if (!res)
						submitCacheAndResubscribe(channelName);
					else
						Log.i(TAG, "connectAndListen: Now requesting with count=" + counter.count + "; offset=" + counter.offset);
					return res;
				})
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe();
		return subject;
	}

	/**
	 * Sends simple chat message, preprocessing it before adding to message queue
	 * @param channelName id of the chat to send message to
	 * @param message text of the message
	 */
	public void sendMessage(String channelName, String message) {
		ChatMessage msg = new ChatMessage(channelName, message);
		sendMessage(channelName, msg);
	}

	/**
	 * Sends custom location, preprocessing it, before adding to the message queue
	 * @param channelName id of the chat to send location to
	 * @param location location you wish to share
	 */
	public void sendMessage(String channelName, UsersLocation location) {
		ChatLocationMessage message = new ChatLocationMessage(channelName, "My location", location.getLat(), location.getLng());
		sendMessage(channelName, message);
	}

	/**
	 * Sends photo to the user, preprocessing message and uploading photo, before adding to the message queue
	 * @param channelName id of the chat to send photo (link) to
	 * @param photo File with photo you wish to upload
	 */
	public void sendMessage(String channelName, File photo) {
		String albumId = channelName;
		Observable.just(true)
				.flatMap(b -> SoulMedia.addPhotoToMyAlbum(albumId, photo))
				.flatMap(res -> {
					Log.i(TAG, "sendMessage: Error? " + res.isErrorHappened());
					Log.i(TAG, "sendMessage: Error obj: " + res.getError());
					if (res.isErrorHappened())
						return Observable.error(new Error(res.getError().getDescription()));
					return Observable.just(res.getResponse());
				})
				.retry()
				.subscribe(res -> {
					ChatPhotoMessage msg = new ChatPhotoMessage(channelName, "Me and my friend", res.getPhoto().getId(), albumId);
					sendMessage(channelName, msg);
				});
	}

	/**
	 * Sends message directly to the message queue.
	 * <p><b>IMPORTANT: </b>If you send complex message types (PHOTO, LOCATION) - make sure you
	 * use boxed methods, like sendMessage(channelName, location) or prepare ChatMessage by yourself.
	 * Messages sent with this method aren't preprocessed. </p>
	 * @param channelName chat id of the message you want to send to
	 * @param message {@link ChatMessage} itself. SDK ensures only time and sender.
	 */
	public void sendMessage(String channelName, ChatMessage message) {
		long serverTime = SoulSystem.getServerTime();
		message.setTimestamp(serverTime);
		message.setUserId(userId);

		if (message.getType() == ChatMessage.Type.DELIVERY || message.getType() == ChatMessage.Type.READ) {
			sender.send(channelName, message);
			return;
		}
		// setting id for regular message
		message.setId(++myLastMessageId);

		String companionIdForPush = partners.get(channelName);
		saveMessageInfo(channelName, message);

		sender.send(channelName, message);

		sender.send(companionIdForPush, message);
	}

	public void unsubscribe(String channelName) {
		sender.setSendingAllowed(false, channelName);
	}

	public Observable<SoulResponse<ChatMessage>> getMessageHistory(String channelName, Counter counter) {
		PublishSubject<SoulResponse<ChatMessage>> subject = PublishSubject.create();
		chatManager.history(channelName, counter.lastMessageTimeToken, counter.count, new ChatCallback<JSONArray>(
				(channel, arr) -> {
					//Thread.sleep(10000);
					JSONArray msgs = arr.getJSONArray(0);
					counter.lastMessageTimeToken = arr.getLong(1);
					JsonArray array = new Gson().fromJson(msgs.toString(), JsonArray.class);

					Observable.from(array)
							.map(JsonElement::toString)
							.map(ChatMessageFabric::create)
							.map(SoulResponse::new)
							.subscribe(subject);
				},
				(channel, err) -> {
					subject.onNext(createError(err.getErrorString(), SoulError.NON_SDK_ERROR));
					subject.onCompleted();
					System.out.println(err.toString());
				}
		));
		return subject
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());
	}

	private Observable<ChatMessage> handleSubscription(String channelName) {
		PublishSubject<ChatMessage> subject = PublishSubject.create();
		try {
			chatManager.subscribe(channelName, new ChatCallback<JSONObject>((channel, object) -> {
				ChatMessage msg = ChatMessageFabric.create(object.toString());
				Log.i(TAG, "handleSubscription: Got message from chat: " + msg);
				Log.i(TAG, "handleSubscription: Message type: " + msg.getType());
				cacheIncoming(channelName, msg);
				subject.onNext(msg);
			}));
		} catch (PubnubException e) {
			e.printStackTrace();
		}

		return subject;
	}

	private PublishSubject<SoulResponse<ChatMessage>> getInputChannel(String channelName) {
		synchronized (this) {
			PublishSubject<SoulResponse<ChatMessage>> sub = inputChannels.get(channelName);
			if (sub == null || sub.hasCompleted()) {
				sub = PublishSubject.create();
				inputChannels.put(channelName, sub);
			}

			return sub;
		}
	}

	private void submitCacheAndResubscribe(String channelName) {
		MessageQueue<ChatMessage> msg = getMessageCache(channelName);
		// save data to storage
		if (msg.size() > 0) {
			Log.i(TAG, "submitCacheAndResubscribe: Loaded something new, saving...");
			saveMessageInfo(channelName, msg.getNewest());
		}

		Log.i(TAG, "submitCacheAndResubscribe: Cache: " + msg);
		Log.i(TAG, "submitCacheAndResubscribe: My last message id: " + myLastMessageId);

		sender.setSendingAllowed(true, channelName);
		sender.setSendingAllowed(true, partners.get(channelName));

		// TODO: can leak a message during re-subscription, but probability is very low
		chatManager.unsubscribe(channelName);
		Observable.concat(Observable.from(msg), handleSubscription(channelName))
				.onBackpressureBuffer()
				.subscribeOn(Schedulers.computation())
				.observeOn(AndroidSchedulers.mainThread())
				.map(SoulResponse::new)
				.subscribe(getInputChannel(channelName).toSerialized());
	}

	private Object cache(String channelName, List<ChatMessage> messages) {
		synchronized (this) {
			Log.i(TAG, "cache: Got msgs: " + messages);
			MessageQueue<ChatMessage> msgCache = getMessageCache(channelName);
			Collections.reverse(messages);
			Observable.from(messages)
					.map(msg -> {
						loadMore = loadMore && !msg.equals(savedMessage);
						if (msg.getUserId().equals(userId) && msg.getId() > myLastMessageId)
							myLastMessageId = msg.getId();
						return msg;
					})
					.filter(message -> !message.equals(savedMessage))
					.forEach(msgCache::add);
		}

		return messages;
	}

	private void saveMessageInfo(String channelName, ChatMessage message) {
		Log.i(TAG, "saveMessageInfo: Saving message with id=" + message.getId());
		String messageJson = new Gson().toJson(message);
		SoulStorage.save(SoulStorage.CHAT_MESSAGE_PREFIX + channelName, messageJson);
		SoulStorage.save(SoulStorage.CHAT_ID_PREFIX + channelName, myLastMessageId + "");
	}

	private void cacheIncoming(String channelName, ChatMessage message) {
		synchronized (this) {
			MessageQueue<ChatMessage> msgCache = getMessageCache(channelName);
			msgCache.addNewest(message);
		}
	}

	private MessageQueue<ChatMessage> getMessageCache(String channelName) {
		synchronized (this) {
			MessageQueue<ChatMessage> msgCache = cache.get(channelName);
			if (msgCache == null) {
				msgCache = new MessageQueue<>();
				cache.put(channelName, msgCache);
			}

			return msgCache;
		}
	}

	private boolean needLoadMoreMessages(Counter counter) {
		Log.i(TAG, "needLoadMoreMessages: Counter status: " + counter);
		Log.i(TAG, "needLoadMoreMessages: Load more: " + loadMore);
		counter.offset += counter.count;
		counter.count = (counter.count * 2 < 100) ? counter.count * 2 : 100;
		return loadMore && counter.lastMessageTimeToken > 0;
	}

	private <T, R> boolean filterAndNotify(PublishSubject<SoulResponse<T>> subject, SoulResponse<R> response) {
		if (response.isErrorHappened()) {
			SoulError err = response.getError();
			subject.onNext(new SoulResponse<>(new SoulError(err.getDescription(), err.getCode())));
			subject.onCompleted();
			return false;
		}

		return true;
	}

	private <T> SoulResponse<T> createError(String description, int errCode) {
		return new SoulResponse<>(new SoulError(description, errCode));
	}

	private <T> SoulResponse<T> createResponse(T data) {
		return new SoulResponse<>(data);
	}

	private class Counter {
		long lastMessageTimeToken;
		int count;
		int offset;

		public Counter() {
			this.lastMessageTimeToken = -1;
			this.count = 1;
			this.offset = 0;
		}

		@Override
		public String toString() {
			return new Gson().toJson(this);
		}
	}
}
