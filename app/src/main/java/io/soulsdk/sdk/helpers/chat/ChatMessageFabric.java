package io.soulsdk.sdk.helpers.chat;

import io.soulsdk.model.dto.ChatDeliveryMessage;
import io.soulsdk.model.dto.ChatLocationMessage;
import io.soulsdk.model.dto.ChatMessage;
import io.soulsdk.model.dto.ChatPhotoMessage;
import io.soulsdk.model.dto.ChatReadMessage;

/**
 * @author kitttn
 */
public class ChatMessageFabric {
	public static ChatMessage create(String json) {
		// checking delivery and read messages at first
		if (json.contains("\"d\":"))
			return ChatDeliveryMessage.createFromJson(json);

		if (json.contains("\"rt\":"))
			return ChatReadMessage.createFromJson(json);

		// okay, it seems everything is good and it is usual message
		if (json.contains("\"pa\":"))
			return ChatPhotoMessage.createFromJson(json);
		if (json.contains("\"lat\":"))
			return ChatLocationMessage.createFromJson(json);
		return ChatMessage.createFromJson(json);
	}
}
