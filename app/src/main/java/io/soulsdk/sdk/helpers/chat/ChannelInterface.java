package io.soulsdk.sdk.helpers.chat;

import com.pubnub.api.PubnubException;

/**
 * @author kitttn
 */
interface ChannelInterface<T, R> {
	void execute(T channel, R object) throws Exception, PubnubException;
}
