package io.soulsdk.sdk;

import android.content.Context;

import io.soulsdk.network.adapter.NetworkAdapter;
import io.soulsdk.network.adapter.RetrofitAdapter;
import io.soulsdk.network.managers.AuthCredentials;
import io.soulsdk.sdk.helpers.ServerAPIHelper;
import io.soulsdk.util.storage.SoulStorage;

/**
 * Initial class. Start using the SDK with this class.
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */

public class SoulSDK {

    private SoulSDK() {
    }

    /**
     * The initial method. Start using the SDK with this method. In this method all Soul Modules
     * will be initialized: {@link SoulAuth}, {@link SoulCurrentUser}, {@link SoulMedia},
     * {@link SoulUsers}, {@link SoulReactions}, {@link SoulCommunication}, {@link SoulEvents},
     * {@link SoulPurchases}, {@link SoulSystem}
     *
     * @param apiKey  the api key of your project
     * @param context Android context
     */

    public static void initialize(String apiKey, Context context) {
        SoulStorage.initialize(apiKey, context);
        NetworkAdapter adapter = new RetrofitAdapter(apiKey);
        ServerAPIHelper helper = new ServerAPIHelper(adapter, new AuthCredentials());
        initialize(helper);
    }

    public static void initialize(ServerAPIHelper helper) {
        SoulConfigs.initialize(helper.getAdapter());
        SoulAuth.initialize(helper);
        SoulCurrentUser.initialize(helper);
        SoulMedia.initialize(helper);
        SoulUsers.initialize(helper);
        SoulReactions.initialize(helper);
        SoulCommunication.initialize(helper);
        SoulEvents.initialize(helper);
        SoulPurchases.initialize(helper);
    }

}
