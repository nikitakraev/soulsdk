package io.soulsdk.model.requests;

import io.soulsdk.model.general.GeneralRequest;

/**
 * HTTP-request body object for patching a photo
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */
public class PatchPhotoREQ  extends GeneralRequest {

    private Float expiresTime;
    private boolean mainPhoto;

    public Float getExpiresTime() {
        return expiresTime;
    }

    public void setExpiresTime(Float expiresTime) {
        this.expiresTime = expiresTime;
    }

    public boolean isMain() {
        return mainPhoto;
    }

    public void setMain(boolean main) {
        this.mainPhoto = main;
    }
}
