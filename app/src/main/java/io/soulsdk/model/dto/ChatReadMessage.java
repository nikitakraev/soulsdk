package io.soulsdk.model.dto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * @author kitttn
 */
public class ChatReadMessage extends ChatMessage {
	@SerializedName("rt")
	double readAt;

	public ChatReadMessage(ChatMessage whichIsRead, double readAt) {
		super("", "");
		this.readAt = readAt;
		setUserId(whichIsRead.getUserId());
		setTimestamp(whichIsRead.getTimestamp());
	}

	@Override
	public Type getType() {
		return Type.READ;
	}

	public static ChatReadMessage createFromJson(String json) {
		return new Gson().fromJson(prepareJSON(json), ChatReadMessage.class);
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}