package io.soulsdk.model.dto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author kitttn
 */
public class ChatMessage {

	public enum Type {
		SIMPLE, PHOTO, LOCATION, DELIVERY, READ
	}

	@SerializedName("m")
	private String message = "";
	@SerializedName("t")
	private double timestamp = 0;
	@SerializedName("id")
	private long id = 0;
	@SerializedName("u")
	private String userId = "";

	private String chatId = "";

	private class GCMMessage {
		@SerializedName("id")
		private long id = 0;
		@SerializedName("u")
		private String userId = "";
		@SerializedName("ch")
		private String chatId = "";

		public GCMMessage(long id, String userId, String chatId) {
			this.id = id;
			this.userId = userId;
			this.chatId = chatId;
		}
	}

	public ChatMessage(String chatId, String message) {
		this.message = message;
		this.chatId = chatId;
	}

	public static ChatMessage createFromJson(String jsonString) {
		return new Gson().fromJson(prepareJSON(jsonString), ChatMessage.class);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(double timestamp) {
		this.timestamp = timestamp;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId != null ? userId : "";
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Type getType() {
		return Type.SIMPLE;
	}

	@Override
	public String toString() {
		JsonObject object = prepareMessage();
		object.add("pn_gcm", preparePNGCM());
		return new Gson().toJson(object);
	}

	public JSONObject toJson() {
		try {
			return new JSONObject(toString());
		} catch (JSONException e) {
			throw new Error(e.getMessage());
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ChatMessage) {
			ChatMessage msg = ((ChatMessage) o);
			return msg.getId() == getId() && msg.getUserId().equals(getUserId());
		}
		return super.equals(o);
	}

	static String prepareJSON(String unprepeared) {
		String jsonString = unprepeared.replace("\\", "");
		if (jsonString.startsWith("\""))
			jsonString = jsonString.substring(1, jsonString.length() - 1);

		return jsonString;
	}

	public JsonObject preparePNGCM() {
		JsonObject data = new JsonObject();
		data.add("data", new Gson().toJsonTree(new GCMMessage(id, userId, chatId)));
		return data;
	}

	public JsonObject prepareMessage() {
		Gson gson = new Gson();
		JsonObject object = new JsonObject();
		object.add("m", gson.toJsonTree(message));
		object.add("t", gson.toJsonTree(timestamp));
		object.add("id", gson.toJsonTree(id));
		object.add("u", gson.toJsonTree(userId));

		return object;
	}
}
