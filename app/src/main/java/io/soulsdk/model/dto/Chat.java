package io.soulsdk.model.dto;

/**
 * Chat entity
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */

public class Chat {

    private String id;
    private String partnerId;
    private float expiresTime;
    private boolean deletedByMe;
    private boolean deletedByPartner;
    private String channelName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public float getExpiresTime() {
        return expiresTime;
    }

    public void setExpiresTime(float expiresTime) {
        this.expiresTime = expiresTime;
    }

    public boolean isDeletedByMe() {
        return deletedByMe;
    }

    public void setDeletedByMe(boolean deletedByMe) {
        this.deletedByMe = deletedByMe;
    }

    public boolean isDeletedByPartner() {
        return deletedByPartner;
    }

    public void setDeletedByPartner(boolean deletedByPartner) {
        this.deletedByPartner = deletedByPartner;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
