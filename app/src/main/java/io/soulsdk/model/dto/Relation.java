package io.soulsdk.model.dto;

/**
 * Relation entity
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */
public class Relation {

    public Relation() {
        liking = new ReactionValue();
        blocking = new ReactionValue();
    }

    private ReactionValue liking;
    private ReactionValue blocking;

    public ReactionValue getLiking() {
        return liking;
    }

    public void setLiking(ReactionValue liking) {
        this.liking = liking;
    }

    public ReactionValue getBlocking() {
        return blocking;
    }

    public void setBlocking(ReactionValue blocking) {
        this.blocking = blocking;
    }

}
