package io.soulsdk.model.dto;

import io.soulsdk.model.general.SoulParameterObject;

/**
 * Filterable variables of User
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */

public class Filterable extends SoulParameterObject {

    private Boolean banned;
    private Double availableTill;
    private UsersLocation location;
    private String gender;
    private String lookingForGender;

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        setProperty("banned", banned);
        this.banned = banned;
    }

    public double getAvailableTill() {
        return availableTill;
    }

    public void setAvailableTill(double availableTill) {
        setProperty("availableTill", availableTill);
        this.availableTill = availableTill;
    }

    public UsersLocation getLocation() {
        return location;
    }

    public void setLocation(UsersLocation location) {
        setProperty("location", location);
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        setProperty("gender", gender);
        this.gender = gender;
    }

    public String getLookingForGender() {
        return lookingForGender;
    }

    public void setLookingForGender(String lookingForGender) {
        setProperty("lookingForGender", lookingForGender);
        this.lookingForGender = lookingForGender;
    }

}
