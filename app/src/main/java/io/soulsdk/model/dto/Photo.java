package io.soulsdk.model.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Photo entity
 *
 * @author Buiarov Uirii
 * @version 0.15
 * @since 28/03/16
 */

public class Photo {

    private String id;
    private float expiresTime;
    private ImageProperties original;
    @SerializedName("300px")
    private ImageProperties _300px;
    private boolean main;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getExpiresTime() {
        return expiresTime;
    }

    public void setExpiresTime(float expiresTime) {
        this.expiresTime = expiresTime;
    }

    public ImageProperties getOriginal() {
        return original;
    }

    public void setOriginal(ImageProperties original) {
        this.original = original;
    }

    public ImageProperties get_300px() {
        return _300px;
    }

    public void set_300px(ImageProperties _300px) {
        this._300px = _300px;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }
}
