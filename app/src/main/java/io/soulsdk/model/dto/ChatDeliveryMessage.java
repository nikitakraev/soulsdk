package io.soulsdk.model.dto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * @author kitttn
 */
public class ChatDeliveryMessage extends ChatMessage {
	@SerializedName("d")
	ChatMessage delivered;

	public ChatDeliveryMessage(ChatMessage delivered) {
		super("", "");
		this.delivered = delivered;
	}

	@Override
	public Type getType() {
		return Type.DELIVERY;
	}

	public static ChatDeliveryMessage createFromJson(String json) {
		return new Gson().fromJson(prepareJSON(json), ChatDeliveryMessage.class);
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}