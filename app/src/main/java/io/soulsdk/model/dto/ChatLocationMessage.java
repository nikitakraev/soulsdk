package io.soulsdk.model.dto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author kitttn
 */
public class ChatLocationMessage extends ChatMessage {
	private double lat = 0;
	private double lng = 0;

	public ChatLocationMessage(String chatId, String message, double lat, double lng) {
		super(chatId, message);
		this.lat = lat;
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public static ChatLocationMessage createFromJson(String jsonString) {
		return new Gson().fromJson(prepareJSON(jsonString), ChatLocationMessage.class);
	}

	@Override
	public Type getType() {
		return Type.LOCATION;
	}

	@Override
	public String toString() {
		Gson gson = new Gson();
		JsonObject msg = prepareMessage();
		msg.add("pn_gcm", preparePNGCM());
		msg.add("lat", gson.toJsonTree(lat));
		msg.add("lng", gson.toJsonTree(lng));

		return gson.toJson(msg);
	}
}
