package io.soulsdk.model.dto;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * @author kitttn
 */
public class ChatPhotoMessage extends ChatMessage {
	@SerializedName("p")
	private String photoId = "";
	@SerializedName("pa")
	private String photoAlbum = "";

	public ChatPhotoMessage(String chatId, String message, String photoId, String photoAlbum) {
		super(chatId, message);
		this.photoId = photoId;
		this.photoAlbum = photoAlbum;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public String getPhotoAlbum() {
		return photoAlbum;
	}

	public void setPhotoAlbum(String photoAlbum) {
		this.photoAlbum = photoAlbum;
	}

	public static ChatPhotoMessage createFromJson(String jsonString) {
		return new Gson().fromJson(prepareJSON(jsonString), ChatPhotoMessage.class);
	}

	@Override
	public Type getType() {
		return Type.PHOTO;
	}
}
