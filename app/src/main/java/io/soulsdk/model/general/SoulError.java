package io.soulsdk.model.general;

/**
 * General Soul Error
 *
 * @author kitttn
 * @version 0.15
 * @since 28/03/16
 */
public class SoulError {

    public static final int UNKNOWN_ERROR = 0;
    public static final int NETWORK_ERROR = 1;

    public static final int PHONE_WRONG_CODE = 2;
    public static final int PHONE_WRONG_NUMBER = 3;
    public static final int PHONE_AUTHENTICATION_ERROR = 4;
    public static final int PHONE_NO_AUTH_CREDENTIALS = 5;

    public static final int PHOTOS_NO_SUCH_ALBUM = 5;

    public static final int LOCATION_NO_PERNISSIONS = 6;
    public static final int LOCATION_NOT_ENOUGH_SOURCES = 7;

    public static final int APP_PAYMENT_REQUIRED = 8;

    public static final int NO_LOGIN_CREDENTIALS = 9;

    public static final int ON_MAIN_THREAD_EXCEPTION = 10;
    public static final int SDK_IS_NOT_INITIALIZED = 11;

    public static final int NON_SDK_ERROR = 0xffff;

    private String description = "";
    private int code = 0;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public SoulError(String description, int code) {
        this.description = description;
        this.code = code;
    }
}
